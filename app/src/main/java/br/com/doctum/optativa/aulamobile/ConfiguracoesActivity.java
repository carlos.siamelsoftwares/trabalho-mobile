package br.com.doctum.optativa.aulamobile;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ConfiguracoesActivity extends AppCompatActivity {

    EditText textUsuario,textSenha,textNovaSenha,textConfSenha;
    String usuario,senha,novaSenha,confSenha;
    Button atualizar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracoes);

        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Configurações");

        textUsuario = findViewById(R.id.campoUsuario);
        textSenha = findViewById(R.id.campoSenha);
        textNovaSenha = findViewById(R.id.novaSenha);
        textConfSenha = findViewById(R.id.confSenha);

        atualizar = findViewById(R.id.atualizar);

        atualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if(validarDados()){
                   finish();
               }
            }
        });
    }

    public Boolean validarDados(){

        usuario = textUsuario.getText().toString();
        senha = textSenha.getText().toString();
        novaSenha = textNovaSenha.getText().toString();
        confSenha = textConfSenha.getText().toString();

        if(!usuario.isEmpty()){
            if(!senha.isEmpty()){
                if(!novaSenha.isEmpty()){
                    if(!confSenha.isEmpty()){
                        if(novaSenha.equals(confSenha)){

                            Log.i("alunos","Usuário: " + usuario);
                            Log.i("alunos","Senha: " + senha);
                            Log.i("alunos","Nova senha: " + novaSenha);
                            Log.i("alunos","confirmação de senha: " + confSenha);

                            msg("Veja os dados no Logcat");
                            return true;
                        }else{
                            msg("As senhas não conferem");
                            return false;
                        }

                    }else{
                        msg("Preencha o campo confirmar senha");
                        return false;
                    }
                }else{
                    msg("Preencha o campo nova senha");
                    return false;
                }
            }else{
                msg("Preencha o campo senha");
                return false;
            }
        }else{
            msg("Preencha o campo usuário");
            return false;
        }
    }

    public  void msg(String msg){
        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
    }

}
