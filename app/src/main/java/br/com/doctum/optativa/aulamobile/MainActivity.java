package br.com.doctum.optativa.aulamobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends LifeCycle {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getSupportActionBar();

        Button button = (Button) findViewById(R.id.buttonEntrar);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editTextUser = (EditText) findViewById(R.id.campoUsuario);
                String user = editTextUser.getText().toString();

                EditText editTextSenha = (EditText) findViewById(R.id.campoSenha);
                String senha = editTextSenha.getText().toString();

                if(user.equals("carlos") && senha.equals("123")) {
                   Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                   Bundle params = new Bundle();
                   params.putString("nome", user);
                   intent.putExtras(params);
                   startActivity(intent);
                   finish();

                   Log.i("alunos","Usuário: " + user + " | Senha: " + senha);
                }
                else{
                    alert("Usuário ou senha incorreto");
                }

            }
        });
    }

    public void alert(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

}
