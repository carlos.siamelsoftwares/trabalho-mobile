package br.com.doctum.optativa.aulamobile;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

public class Aluno {

    private String nome;
    private String email;
    private String telefone;
    private int idade;
    private static ArrayList<Aluno> listaAlunos;
    private Activity activity;

    Aluno(Activity activity) {
        this.activity = activity;
        if(listaAlunos == null){
            listaAlunos = new ArrayList<>();
        }
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public void addAluno(Aluno aluno){
        listaAlunos.add(aluno);
    }

    public static ArrayList<Aluno> getListaAlunos() {
        return listaAlunos;
    }


    public Boolean validarDados( String nome, String email, String telefone, String idade){

        if(!nome.isEmpty()){
            if(!email.isEmpty()){
                if(!telefone.isEmpty()){
                    if(!idade.isEmpty()){

                        this.setNome(nome);
                        this.setEmail(email);
                        this.setTelefone(telefone);
                        this.setIdade(Integer.parseInt(idade));
                        this.addAluno(this);

                        this.imprimeLogcat();

                        return true;

                    }else{
                        msg("Preencha o campo idade");
                        return false;
                    }
                }else{
                    msg("Preencha o campo telefone");
                    return false;
                }
            }else{
                msg("Preencha o campo email");
                return false;
            }
        }else{
            msg("Preencha o campo nome");
            return false;
        }
    }

    public void msg(String msg){
        Toast.makeText(this.activity,msg,Toast.LENGTH_SHORT).show();
    }

    public void imprimeLogcat(){
        Log.i("alunos","Nome: " + this.getNome());
        Log.i("alunos","Email: " + this.getEmail());
        Log.i("alunos","Telefone: " + this.getTelefone());
        Log.i("alunos","Idade: " + this.getIdade());
    }

}
