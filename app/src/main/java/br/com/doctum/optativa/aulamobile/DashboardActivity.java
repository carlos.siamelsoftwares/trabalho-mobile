package br.com.doctum.optativa.aulamobile;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class DashboardActivity extends LifeCycle {


    TextView iconCad,iconListar,iconEditar,iconConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Dashboard");

        Bundle bundle = getIntent().getExtras();
       // String nomeUser = bundle.getString("nome");

        iconConfig = findViewById(R.id.iconConfig);
        iconListar= findViewById(R.id.iconListar);
        iconEditar = findViewById(R.id.iconEditar);
        iconCad = findViewById(R.id.iconCad);


        /*TextView textView = (TextView) findViewById(R.id.textShow);
        textView.setText(nomeUser);*/


    }

    public void abrirTela(View view){
        switch (view.getId()){
            case R.id.iconCad:
                startActivity(new Intent(getApplicationContext(),CadastroActivity.class));
                break;

            case R.id.iconEditar:
                startActivity(new Intent(getApplicationContext(),EditarCadastroActivity.class));
                break;

            case R.id.iconConfig:
                startActivity(new Intent(getApplicationContext(),ConfiguracoesActivity.class));
                break;

            case R.id.iconListar:
                if(Aluno.getListaAlunos() != null) {
                    startActivity(new Intent(getApplicationContext(), ListaActivity.class));
                }else {
                    Toast.makeText(getApplicationContext(),"A lista está vazia",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


}
