package br.com.doctum.optativa.aulamobile;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EditarCadastroActivity extends AppCompatActivity {

    EditText textNome,textEmail,textTelefone,textIdade;
    String nome,email,telefone,idade;
    Button editar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_cadastro);

        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Editar Alunos");

        textNome = findViewById(R.id.nome);
        textEmail = findViewById(R.id.email);
        textTelefone = findViewById(R.id.telefone);
        textIdade = findViewById(R.id.idade);

        editar = findViewById(R.id.editar);

        editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                salvar();
            }
        });

    }

    public void salvar(){
        Aluno aluno = new Aluno(this);

        nome = textNome.getText().toString();
        email = textEmail.getText().toString();
        telefone = textTelefone.getText().toString();
        idade = textIdade.getText().toString();


        if(aluno.validarDados(nome,email,telefone,idade)){
            aluno.msg("Veja os dados no LogCat");
            finish();
        }
    }


}
