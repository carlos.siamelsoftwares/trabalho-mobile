package br.com.doctum.optativa.aulamobile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class AlunoAdapter extends ArrayAdapter<Aluno> {

    private List<Aluno> items;

    public AlunoAdapter(Context context, int textViewResourceId, List<Aluno> items) {
        super(context, textViewResourceId, items);
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            Context ctx = getContext();
            LayoutInflater vi = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_lista, null);
        }
        Aluno aluno = items.get(position);
        if (aluno != null) {
            ((TextView) v.findViewById(R.id.nome)).setText(aluno.getNome());
            ((TextView) v.findViewById(R.id.email)).setText("Email: "+aluno.getEmail());
            ((TextView) v.findViewById(R.id.telefone)).setText("Tel: "+aluno.getTelefone());
            ((TextView) v.findViewById(R.id.idade)).setText("Idade: "+String.valueOf(aluno.getIdade() + " anos"));
            ((ImageView) v.findViewById(R.id.imagem)).setImageResource(R.drawable.ic_aluno_cinza_24dp);
        }
        return v;
    }




}