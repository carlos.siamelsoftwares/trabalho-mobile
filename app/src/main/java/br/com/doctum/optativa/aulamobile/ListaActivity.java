package br.com.doctum.optativa.aulamobile;

import android.content.DialogInterface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AndroidException;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListaActivity extends AppCompatActivity {

    ArrayList<Aluno> alunos;
    ListView listaAlunos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        ActionBar  actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Lista de Alunos");

        alunos = Aluno.getListaAlunos();
        listaAlunos = findViewById(R.id.listaAlunos);


        AlunoAdapter adapter = new AlunoAdapter(getBaseContext(), R.layout.item_lista, alunos);
        listaAlunos.setAdapter(adapter);

        listaAlunos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Aluno aluno = (Aluno) listaAlunos.getItemAtPosition(i);
                aluno.imprimeLogcat();
                alerta(aluno);

            }
        });

    }

    private void alerta(Aluno aluno) {
        AlertDialog alerta;

        //Cria o gerador do AlertDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //define o titulo
        builder.setTitle("Aluno");
        //define a mensagem
        builder.setMessage("Nome: " +aluno.getNome() + "\nTelefone: " + aluno.getTelefone() + "\nEmail: " + aluno.getEmail() + "\nIdade: " + aluno.getIdade() + " anos");
        //define um botão como positivo
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Log.i("alunos","Alerta fechado");
            }
        });
        //cria o AlertDialog
        alerta = builder.create();
        //Exibe
        alerta.show();
    }



}
