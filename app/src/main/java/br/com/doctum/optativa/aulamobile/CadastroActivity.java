package br.com.doctum.optativa.aulamobile;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CadastroActivity extends AppCompatActivity {

    EditText textNome,textEmail,textTelefone,textIdade;
    String nome,email,telefone,idade;
    Button cadastrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Cadastro de Alunos");

        textNome = findViewById(R.id.nome);
        textEmail = findViewById(R.id.email);
        textTelefone = findViewById(R.id.telefone);
        textIdade = findViewById(R.id.idade);

        cadastrar = findViewById(R.id.cadastrar);

        cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                salvar();
            }
        });
    }


    public void salvar(){
        Aluno aluno = new Aluno(this);

        nome = textNome.getText().toString();
        email = textEmail.getText().toString();
        telefone = textTelefone.getText().toString();
        idade = textIdade.getText().toString();

        if(aluno.validarDados(nome,email,telefone,idade)){
            aluno.msg("Aluno cadastrado com sucesso");
            finish();
        }
    }




}
